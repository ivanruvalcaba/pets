##
# Filename: testCat.nim
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 08 ago 2020 08:45:49
# Last Modified: 10 ago 2020 08:57:54
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
##

import unittest
import strformat

import pets/cat

suite "Test suite for ``Cat`` class with default values.":
  setup:
    # Create a new instance of the ``Cat`` object with default values.
    let cat = newCat()

  test "The cat's name is 'cat'.":
    check( fmt( "{ cat.name }" ) == "cat" )

  test "The cat's color is 'black'.":
    check( fmt( "{ cat.color }" ) == "black" )

  test "The cat says 'meow!'.":
    check( fmt( "{ cat.says }" ) == "meow!" )

  test "The cat's age is 1 year old.":
    check( cat.age == 1 )

  test "The cat owner is 'nobody'.":
    check( fmt( "{ cat.owner }" ) == "nobody" )

suite "Test suite for ``Cat`` class with custom values.":
  setup:
    # Create a new instance of the ``Cat`` object with custom values.
    #
    # @param {string} name  - The cat's name.
    # @param {string} color - The cat's color.
    # @param {string} says  - The cat saying something.
    # @param {number} age   - The cat's age.
    # @param {string} owner - The cat's owner.
    let cat = newCat( "Luna", "white", "meow!!!", 3, "Iván" )

  test "The cat's name is 'Luna'.":
    check( fmt( "{ cat.name }" ) == "Luna" )

  test "The cat's color is 'white'.":
    check( fmt( "{ cat.color }" ) == "white" )

  test "The cat says 'meow!!!'.":
    check( fmt( "{ cat.says }" ) == "meow!!!" )

  test "The cat's age is 3 years old.":
    check( cat.age == 3 )

  test "The cat owner is 'Iván'.":
    check( fmt( "{ cat.owner }" ) == "Iván" )

suite "Test suite for ``Cat`` class with partial custom values.":
  setup:
    # Create a new instance of the ``Cat`` object with partial custom
    # values. Note that the argument/parameter order does not matter.
    #
    # @param {string} name  - The cat's name.
    # @param {string} color - The cat's color.
    # @param {string} says  - The cat saying something.
    # @param {number} age   - The cat's age.
    # @param {string} owner - The cat's owner.
    let cat = newCat(
                      name = "Cookie",
                      color = "orange",
                      owner = "Jean"
                      )

  test "The cat's name is 'Cookie'.":
    check( fmt( "{ cat.name }" ) == "Cookie" )

  test "The cat's color is 'orange'.":
    check( fmt( "{ cat.color }" ) == "orange" )

  test "The cat says 'meow!'.":
    check( fmt( "{ cat.says }" ) == "meow!" )

  test "The cat's age is 1 year old.":
    check( cat.age == 1 )

  test "The cat owner is 'Jean'.":
    check( fmt( "{ cat.owner }" ) == "Jean" )
