##
# Filename: testCustomer.nim
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 10 ago 2020 09:58:41
# Last Modified: 10 ago 2020 11:00:25
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
##

import unittest
import strformat

import pets/customer

suite "Test suite for ``Customer`` class with default values.":
  setup:
    # Create a new instance of the ``Customer`` object with default
    # values.
    let customer = newCustomer()

  test "The customer's name is 'Unknown'.":
    check( fmt( "{ customer.name }" ) == "Unknown" )

  test "The customer's gender is 'Unknown'.":
    check( fmt( "{ customer.gender }" ) == "Unknown" )

  test "The customer's age is 5.":
    check( customer.age == 5 )

  test "The customer's address is 'Unknown'.":
    check( fmt( "{ customer.address }" ) == "Unknown" )

  test "The customer's phone is 'Unknown'.":
    check( fmt( "{ customer.phone }" ) == "Unknown" )

  test "The customer's email is 'Unknown'.":
    check( fmt( "{ customer.email }" ) == "Unknown" )

suite "Test suite for ``Customer`` class with custom values.":
  setup:
    # Create a new instance of the ``Customer`` object with custom values.
    #
    # @param {string} name    - The customer's name.
    # @param {string} gender  - The customer's gender.
    # @param {number} age     - The customer's age.
    # @param {string} address - The customer's address.
    # @param {string} phone   - The customer's phone.
    # @param {string} email   - The customer's email.
    let customer = newCustomer( "Iván", "Male", 39,
                                "20 de Noviembre #6", "5566778899",
                                "foo@bar.com" )

  test "The customer's name is 'Iván'.":
    check( fmt( "{ customer.name }" ) == "Iván" )

  test "The customer's gender is 'Male'.":
    check( fmt( "{ customer.gender }" ) == "Male" )

  test "The customer's age is 39.":
    check( customer.age == 39 )

  test "The customer's address is '20 de Noviembre #6'.":
    check( fmt( "{ customer.address }" ) == "20 de Noviembre #6" )

  test "The customer's phone is '5566778899'.":
    check( fmt( "{ customer.phone }" ) == "5566778899" )

  test "The customer's email is 'foo@bar.com'.":
    check( fmt( "{ customer.email }" ) == "foo@bar.com" )

suite "Test suite for ``Customer`` class with partial custom values.":
  setup:
    # Create a new instance of the ``Customer`` object with partial
    # custom values. Note that the argument/parameter order does not
    # matter.
    #
    # @param {string} name    - The customer's name.
    # @param {string} gender  - The customer's gender.
    # @param {number} age     - The customer's age.
    # @param {string} address - The customer's address.
    # @param {string} phone   - The customer's phone.
    # @param {string} email   - The customer's email.
    let customer = newCustomer( name = "Jean", gender = "Male",
                                age = 14 )

  test "The customer's name is 'Jean'.":
    check( fmt( "{ customer.name }" ) == "Jean" )

  test "The customer's gender is 'Male'.":
    check( fmt( "{ customer.gender }" ) == "Male" )

  test "The customer's age is 14.":
    check( customer.age == 14 )

  test "The customer's address is 'Unknown'.":
    check( fmt( "{ customer.address }" ) == "Unknown" )

  test "The customer's phone is 'Unknown'.":
    check( fmt( "{ customer.phone }" ) == "Unknown" )

  test "The customer's email is 'Unknown'.":
    check( fmt( "{ customer.email }" ) == "Unknown" )
