##
# Filename: cat.nim
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 08 ago 2020 08:31:32
# Last Modified: 10 ago 2020 09:35:44
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
##

## Define the ``Cat`` object.
type
  Cat = ref object
    name*: string
    color*: string
    says*: string
    age*: Natural
    owner*: string

## Create and returns a new instance of the ``Cat`` object.
proc newCat*( name: string = "cat", color: string = "black",
              says: string = "meow!", age: Natural = 1,
              owner: string = "nobody" ): Cat =
  # Note: The type inference works for parameters with default values so
  # technically there would be no need to do the following:
  #
  # ``name:string = "cat"``
  #
  # however, I am a true believer in the adage: "explicit is better than
  # implicit".
  Cat(
    name: name,
    color: color,
    says: says,
    age: age,
    owner: owner
  )
