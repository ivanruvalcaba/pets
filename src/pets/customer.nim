##
# Filename: customer.nim
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 10 ago 2020 09:38:44
# Last Modified: 10 ago 2020 10:22:01
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
##

## Define the ``Customer`` object.
type
  Customer = ref object
    name*: string
    gender*: string
    age*: Natural
    address*: string
    phone*: string
    email*: string

## Create and returns a new instance of the ``Customer`` object.
proc newCustomer*(name: string = "Unknown", gender: string = "Unknown",
                  age: Natural = 5, address: string = "Unknown",
                  phone: string = "Unknown", email: string = "Unknown"
                  ): Customer =
  # Note: The type inference works for parameters with default values so
  # technically there would be no need to do the following:
  #
  # ``name:string = "Unknown"``
  #
  # however, I am a true believer in the adage: "explicit is better than
  # implicit".
  Customer(
    name: name,
    gender: gender,
    age: age,
    address: address,
    phone: phone,
    email: email
  )
