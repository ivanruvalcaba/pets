##
# Filename: pets.nim
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 08 ago 2020 08:41:02
# Last Modified: 10 ago 2020 13:23:18
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
##

import strformat

import pets/customer
import pets/cat

let programDescription =  """
pets, a.k.a: pet(s) or "pet shop", is a toy package intended to simulate
a pet shop using the object-oriented programming paradigm and whose main
purpose is to give a try the features of the nim language (i.e. "My baby
steps through nim lang").

## Running the tests

From the command line run:

nimble test

## Author

* Iván Ruvalcaba — Initial work — https://gitlab.com/ivanruvalcaba

## License

Copyright (c) 2020 — Iván Ruvalcaba. This project is licensed under the
GNU Lesser General Public License version 3 (LGPL-3.0). See the LICENSE
file for details.

Some objects (classes) that are currently implemented are:
"""
let objCat = newCat( "Luna", "white", "meow!!!", 3, "Iván" )
let objCustomer = newCustomer( "Iván", "Male", 39, "20 de Noviembre #6",
                            "5566778899", "foo@bar.com" )

echo( programDescription )
echo( fmt(
          "* Cat object. Example:\n\t" &
          "cat's name: { objCat.name }\n\t" &
          "cat's color: { objCat.color }\n\t" &
          "cat says: { objCat.says }\n\t" &
          "cat's age: { objCat.age }\n\t" &
          "cat owner: { objCat.owner}"
      )
    )
echo( fmt(
          "* Customer object. Example:\n\t" &
          "customer's name: { objCustomer.name }\n\t" &
          "customer's gender: { objCustomer.gender }\n\t" &
          "customer's age: { objCustomer.age }\n\t" &
          "customer's address: { objCustomer.address }\n\t" &
          "customer's phone: { objCustomer.phone }\n\t" &
          "customer's email: { objCustomer.email }"
      )
    )
