# Package

version       = "0.1.0"
author        = "Iván Ruvalcaba"
description   = "A new awesome nimble package about cute pets in a pet store"
license       = "LGPL-3.0"
srcDir        = "src"

# Dependencies

requires "nim >= 1.2.6"
