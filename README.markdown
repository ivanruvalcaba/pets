[//]: # (Filename: README.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 08 ago 2020 14:03:06)
[//]: # (Last Modified: 10 ago 2020 13:25:12)

# Pets

> A new awesome nimble package about cute pets in a pet store.

pets, a.k.a: pet(s) or "pet shop", is a  nimble *toy* package intended to simulate a pet shop using the object-oriented programming paradigm.

### Motivation

The main motivation of this package is to give a try of the capabilities of the *nim* language, at the same time to become familiar with it (i.e. "My baby steps through nim lang").

### Prerequisites

* *git*
* *nim >= 1.2.6*

## Running the tests

From the command line run:

```
nimble test
```

Example output:

```
[Suite] Test suite for ``Cat`` class with default values.
  [OK] The cat's name is 'cat'.
  [OK] The cat's color is 'black'.
  [OK] The cat says 'meow!'.
  [OK] The cat's age is 1 year old.
  [OK] The cat owner is 'nobody'.

[Suite] Test suite for ``Cat`` class with custom values.
  [OK] The cat's name is 'Luna'.
  [OK] The cat's color is 'white'.
  [OK] The cat says 'meow!!!'.
  [OK] The cat's age is 3 years old.
  [OK] The cat owner is 'Iván'.

[Suite] Test suite for ``Cat`` class with partial custom values.
  [OK] The cat's name is 'Cookie'.
  [OK] The cat's color is 'brown'.
  [OK] The cat says 'meow!'.
  [OK] The cat's age is 1 year old.
  [OK] The cat owner is 'Jean'.
   Success: Execution finished
   Success: All tests passed
```

## Authors

* **Iván Ruvalcaba** — *Initial work* — [ivanruvalcaba](https://gitlab.com/ivanruvalcaba).

## License

Copyright (c) 2020 — Iván Ruvalcaba. This project is licensed under the GNU Lesser General Public License version 3 (LGPL-3.0). See the [LICENSE](LICENSE) file for details.
